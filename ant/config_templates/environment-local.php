<?php 
define('ENVIRONMENT', 'LOCAL');

define('BASEPATH', '/tyrfing.org/src/public_html');
define('BASEURL', 'http://' . $_SERVER["HTTP_HOST"] . BASEPATH . '/');

define( 'CSS_DEBUG', false );
define( 'JS_DEBUG', false );

error_reporting(E_ALL);
ini_set('display_errors', 'On');
?>