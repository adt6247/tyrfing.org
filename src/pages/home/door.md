An Exterior Door Install
========================

Prologue
--------
This tale starts with a decision over 4th of July weekend 2005 to start a
serious effort to do some repairs around my new house. I was working on fixing a
brick foundation and all was going according to plan. While I was at the
hardware store I decided to pickup a new set of door locks. As I believe in the
standard practice of replacing the door locks when you buy a new house. Later in
the week, there was time to do a project, but not enough to lay bricks. This
seemed like the perfect time to look into installing the new door locks. The
first door choice was the primary egress to the house the 1st floor back door.
After removing the old (non-functional) dead bolt, it became quite evident that
the strike plates in this door were mounted into the molding. when I tried to
drill a hole the molding splintered and fell apart. At this Point I decided My
best course of action would be to...

Rehang the Old Door
-------------------
The plan was simple. I would rehang the old door. I purchased wood to build a
new door frame. Then began the first step, demolition. I carefully removed the
old door. Then I started to rip out the door frame. The molding was shot so it
came out. This revealed that there was no plaster behind the molding. The
molding also was to close to the nearby window molding so the window molding had
been cut down. I decide to set aside a piece of the hinge side door molding to
replace the cutup piece of window molding and replace all the modling around the
door with a thinner yet similar molding. The wood of the door frame was removed.
Around the door the siding was stripped to the sheathing. It was a beautiful
rough opening.

So I went about building the new door frame everything was nice and square. The
new hinges were nicely recessed. Everything was perfect except the old door
wasn't really square anymore. the corners had all become rounded with age and it
had obviously been rehung several times before. After much cajoling I managed to
get the door hung. It just would close correctly. I was also less than impressed
with the frame I built all the parts that held it together ended up outside the
house. So I decided to go back to the drawing board search the net for ideas and
just live with the door for the next week or so. This is when I learned: 
**Always use Prehung Doors.** Now as I saw it I had two options I could buy
material more suited for exterior door frames and prehang the door myself then
install it after everything was nice and square. Or I could just buy a new
prehung steel door. The old door was 84 inches which was nice as I'm the
shortest one in the house at 6-2. and I'd have to shrink the rough opening in
order to fit a 80 inch standard door. When I went to the hardware store and the
largest piece of exterior door jab lumber was only 80 inches I decided to cut my
losses and ...

Install a Factory Assembled Prehung Steel Door
----------------------------------------------
I went down to the hardware store and chose a nice door. Similar in style to the
old door and a close in dimensions to my rough opening as possible. My studies
on the web said a mildly proficient carpenter should be able to install a
prehung door in less than two hours. I was confident that the time I set aside
on sunday afternoon would be sufficient and that I'd be done in time to clean up
my kitchen for dinner after 3 weeks of prolonged demolition mess.

Things started simple enough. The old door came right out. The first delay was
in order to remove the threshhold it was decided that we needed to remove the
siding on a wall next to, but perpendicular to the door. As I already removed
some and wanted to change the style of the porch, it wasn't to bad when we found
out this wasn't needed. I added 2x4 headers to bring in the rough opening. I
checked the rough opening for square. Everything looked good. My buddy slid in
the door. So far so good. I shimmed the door as directed in the instructions.
Everything was level and square. I went to open the door. perfect. I went to
close the door. It wouldn't close right. I "convinced" the door to close. had my
buddy take the door out. I figured I shimmed it improperly and tried again.
Still no go. One more time. nothing. Now I began to get a little frustrated and
looked for what where the small differences that might be affecting the door
closing properly. I start measuring everything. Make sure the top is as wide as
the bottom. Make sure the threshold resting on a level roughopening is still
level. This is when I notice the threshold that came preattached to the door
frame was 1/4 inch off on one side. Now I was starting to feel the weight of it
all. The store was closed. I had work the next day.

I decided to pull the door down again and fix the threshold. As soon as we get
the door outside it starts to rain. I pull out the two screws holding the
threshold on line it up with the other side of the door and screwed them back
in. Besides the rain amazingly easy. We now decide to try rehanging the door one
more time. I go to shim it. I still can't quite get the right spacing around the
door. Apparently the door had been held out of square for so long by the
threshold that the frame had become slightly warped. Fine. I figured out what
the appropriate shimming should be opened the door held the shims at the right
depth and screwed in the frame until the screw pulled the frame out to the shim.
This freed the door up for easy opening and closing. I tossed the doorknob and
dead bolt on using the default strike plate holes and called it a weekend. I
didn't have enough caulk to do all around the door. I figured I'd pick up more
on Monday on the way home from work.

The next day I noticed that the door sat a little to far outside my house. This
would make it very difficult to blend into the plaster, and would limit how far
the door would open. At work I decided I'd rehang the door after cutting a thin
margin out of the inch thick sheathing around the door. Then I would push the
door as far into the house as it would go, mounting the door on the 2x4s. Now,
my house is pretty old and the sheathing is as near as I can tell from the
siding I have removed is 1 by 12 running at a 45 degree angle. Cutting an inch
off around the door was more difficult than I thought. A small circular saw
would have been ideal. I ended up going back and forth between a chisel and a
reciprocating saw held at an angle such that it cut the sheathing but not the
framing. I got that out. I had a brother slide the door in. and then I shimmed
in a method similar to what I had done the day before. This install actually
went pretty fast. I caulked around the door nice and tight and called it
installed.

Epilogue
--------
Later when I went back to install the strike plates that came with the new door
knob and deadbolt, I found that the door manufacturer had installed a metal
plate in my way. I wasn't anything a little brute force couldn't convince to get
out of the way.

I'm now very happy with the door. I used some small pieces of drywall to fill in
the missing plaster. When my brothers are done repainting the kitchen, I've got
some nice molding to go up around the door. The window molding repair went off
relatively well. someday I'll probably take pictures of the finished project for
all to marvel at. But not today. I hope this cautionary tell has informed as
well as entertained you. Good Luck.
