Ted's Ultimate Toolbox
======================
This is the list of the portable tools I think everyone should keep on them when
doing home repair. I'll probaly add more decription of there secret value later.
I divide what I keep in my toolbox into two catagories. Tools that I use to do
the jobs. Hardware that get used up by the jobs. I keep this list of tools in my
tool box that I take when I'm going to fix something for friends or family.
However, it would also be a good list for a toolbox somewhere space is limited
like a condo or apartment.

I don't keep powertools in my toolbox, but a good cordless drill should be owned
by everyone with a permanent residence. After that a good circular saw or
recipricating saw are good choices, but that begins the slippery slope to power
mitre saws, table saws, band saws, radial arm saws, routers, etc. It's a
terrible addiction. Plus none of these things are really portable enough to keep
in most general purpose tool boxes.

I also keep a bag of assorted wrenches and jewelers screwdriver in my toolbox,
mostly because they need to be somewhere and they are have proved useful for me
in the past. The only other major thing in my toolbox is my Digital MultiMeter.
I have this because I do mostly house wiring for people and it comes in handy.
Which brings me to my final point, please add stuff to this toolbox for the
projects that YOU do. Maybe you do a lot of network and telephone wiring and
keeping a punch down tool makes sense (when I need one I just make due with the
back  of a utility knife).

Only you know what your key projects are. If you start with this tool box, I
imagine you'll be able to do most things. If you find yourself wishing you had a
portable ratchet set, then add one.

Tools
-----
- Needlenose pliers
- Pliers
- Utility knife
- Screw driver P2
- Screw driver 3/16
- Wire stripper
- Hammer
- Putty knife
- Tape Measure
- Adjustable Wrench
- Pipe Wrench
- Small Hacksaw
- Small Wood Saw
- Pencils
- Small Flashlight
- Toothbrush
- Safety glasses (wrapped in a swiffer sheet)
- Extension cord (atleast polarized preferable grounded)

Hardware
--------
- Electrical Tape
- Teflon Tape
- Duct Tape
- Fiberglass Mesh drywall Tape
- Lightweight Spackle
- Romex
- Twist ties
- Assorted
  - Wirenuts
  - Screws
  - Bolts
  - Nuts
  - Washers
  - Plumbing Washers
  - Drywall Anchors (threaded & straight)

