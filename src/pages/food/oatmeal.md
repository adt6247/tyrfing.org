Instant Chocolate Oatmeal
=========================
For a while I was eating packet oatmeal. I liked it because it was shelf stable and just required hot water to make. I found it was a great quick breakfast while at work. The Water cooler had a hot water tap for tea and such so that was made it easy to make. It also could be kept in my desk without rotting which was good. It comes in many flavors from crazy things with marshmallows for kids all the way to exotic spice flavors for adults.

The problem  with these packets were two fold. The first was the packet oat meal is significantly more expensive than the big cylinders. The second is I couldn't find chocolate. One of the most popular flavors and it just doesn't exist in oatmeal. So did a little research and experimentation and came up with the following recipe:

Ingredients
-----------
- 8 Tsp oatmeal (the increased surface area of instant improved absorption, but just rolled should work as well)
- 5 Tsp fat-free dry milk
- 3 Tsp fortified chocolate milk mix (I've used the Nesquik powder)

Procedure
---------
1. Mix above ingredients.
2. Mix in hot water to desired consistency ( < 1 cup ).
3. Let it soak and combine.
4. Stir and eat.

I've been eating this for a while and it is a good creamy fresh baked chocolate type flavor. Like many chocolate recipes it goes great with nuts. When I have some a crush up a small handful into the oatmeal, delicious.
