Simple Fruit Crisp
==================
I was in the supermarket buying fruit. There next to the apples was several
boxes of fruit crisp mix. I looked in the picture and figured it would be easy
to make from scratch I checked the back of the box and the ingredients were just
what I'd figure. So here is my fruit (I've tried peaches and apples) crisp
recipe:

Ingredients
-----------
- Fruit (for 1p = 1cup I found 12 medium apples work nicely)
- 1p oatmeal
- 1p flour (preferably whole wheat)
- 1p brown sugar
- 1p white sugar
- 1p softened butter or oil (I prefer canola)
- cinnamon and nutmeg to taste (for 1p = 1c I went ~1/2 t cin, ~1/4 t nutmeg).

Procedure
---------
1. Preheat oven to 375&deg; F.
2. Slice fruit into greased pan.
3. Mix remaining ingredients into a crumbly paste.
4. Sprinkle mixture over top of sliced fruit.
5. Bake uncovered for ~30 min.

I highly recommend letting it sit so that the flavors can blend and the oatmeal
can absorb some liquid. As you have probably figured out it goes great warmed
with vanilla ice cream.
