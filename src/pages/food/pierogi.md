Pierogi
=======

This is the recipe as orginally recorded with added notes.

## Dough ##

### Basic Dough ###

- 1 egg
- 1/2 cup milk or water
- 1/2 tsp. salt
- 2 cups flour

#### Procedure ####
1. Beat Egg, liquid and salt together.
1. Add flour and mix well.
1. Turn out on floured board and knead well. Dough should be soft and smooth.
1. Cover dough with a warm bowl and let rest for 10 minutes.
1. Divide dough and roll thin. (Keep unused portion covered so it will not dry out.)
1. Cut dough into 2 inch or 3 inch squares for smaller pierogi, or use a coffee cup and cut the circles for larger size.
1. Place small amount of filling in center.
1. Fold small pierogi into triangles and circles in half. 
1. Pinch firmly to seal edges. Use finger or fork.
1. Place sealed pierogi on a lightly floured surface and cover with a towel.
1. Bring large pot of water to a boil.
1. Place pierogi into boiling water gently, do not crowd.
1. Stir slowly with a wooden spoon once or twice to keep them from sticking together.
1. When water returns to a boil, time for 5 to 8 minutes.
1. Remove from water with a slotted spoon and rinse in colander.

Pierogi may be fried in butter or margarine, or served warm with melted butter.

### Cream Cheese Dough ###
- 1 small (3 oz) cream cheese 
- 2 cups flour
- 1/2 cup milk
- 1 egg

See directions above. Add slightly more flour, if needed to make dough 
smooth.  Be careful....too much flour makes the dough tough.

Makes approx. 3 dozen.

### Mashed Potato Dough ###
- 1/2 mashed potatoes
- 1 egg (beaten)
- 1/2 tsp salt
- 1/2 cup milk or water
- 2 cups flour

#### Procedure ####
1. Mix ingredients in order given.
2. Add more flour, if necessary, to make dough soft and smooth. 
3. Follow directions above.

## Fillings ##

### Sauerkraut ###
One quart sauerkraut makes about 80 small pierogi.
1. Drain sauerkraut and rinse in cold water.
2. Place in saucepan, cover with cold water and cook 5 to 10 
   minutes....depending on how tart you like it.
3. Drain well.
4. When cool squeeze excess water with hands.
5. Cut with kitchen shears or chop with a knife.
6. Saute  1 small onion in butter or margarine (4 T.) until soft.  You can add more butter if you like.
7. Add to sauerkraut and mix well.
8. Season with salt and pepper to taste.

Polish style sauerkraut has caraway seeds. You can also add mushrooms...chopped along with the
chopped onion.

### Prune ###
Use whole cooked prunes or buy prune filling. One pound carton of filling
makes about 80 small pierogi.

Or use bite size. Bring to a boil and then let let set to plump up. Drain.

### Mashed Potatoes and Cheese ###
- 1 1/2 cups dry instant potato buds
- 1 1/4 cups boiling water
- 4-5 slices American cheese
- salt and pepper...if desired

#### Procedure ####
1. Melt cheese in boiling water and add to instant potatoes. Mix well.
2. Season to taste. (Remember....cheese is salty)

This recipe makes about 65 small pierogi.

Alternate:
- 3 cups mashed potatoes
- 8 oz cream cheese
- 1 cup shredded cheddar

### Cottage Cheese ###
- 1 lb. carton dry cottage cheese or Baker's cheese
- 1 beaten egg yolk
- 1/4 cup sugar to taste (too much makes it watery)
- dash of salt

#### Procedure ####
1. Force cheese thru sieve or mash with a fork.
2. Mix with other ingredients.
3. Makes about 80 small pierogi.

## ENJOY! ##
- Melt margarine in 13"x9" pan, and mix pierogi around in it.
- Then can cool and freeze.
