Chewy All-Oatmeal Cookies
=========================
This recipe is the progression of my chewy chocolate chip cookie recipe. I
wanted to try to craft an all-oatmeal no flour cookie. I wanted it to stay
chewy.  One of the keys to this recipe was getting the oatmeal really gummy to
replace the lack of gluten this is done by the key part of the procedure in
which  the batter is allowed to rest for a while and let the oatmeal absorb the
water. This could probably be forced to happen faster by cooking the water into
the oatmeal, but I have always just let it sit.

Note: This recipe is vegan and gluten-free (if gluten-free oats and mix ins are
used).

Ingredients
-----------
- 4c oats (I've mostly used quick, but old-fashioned will work)
- 1c white sugar
- 0.5c brown sugar
- 0.5t salt
- 1c water
- 0.5c oil
- 1t baking soda
- 1.5t vanilla
- 1.5c mix-ins (I like chocolate chips, but have used chocolate chunks and 
  candy pieces. I'm also considering trying some dried fruit or nuts)

Procedure
---------
1. Preheat oven to 350 degrees.
2. Mix oats, sugars, salt, and water together in a large mixing bowl.
3. Let completely combined ingredients, soak for 1 hour so that the oatmeal 
   can fully rehydrate.
4. Mix in the rest of the ingredients into the fully hydrated oatmeal.
5. Drop tablespoons of the cookie dough onto a cookie sheet.
6. Bake for 12-15 min.

Up to 1 cup of the oats can be reserved and mixed in after the rehydration
period to get large whole oat pieces in the final cookie if desired.
