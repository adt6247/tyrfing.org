Tetrazini
=========

When I was little, quite possibly my favorite dish that my mother made was
turkey tetrazini. I recall watching her and it all seeming so easy. I have spent
a long time experimenting with various combinations in order to reach the
perfect recipe. I recall my mother's recipe required cooked turkey, condensed
cream of mushroom soup, spaghetti and grated Parmesan cheese. She also made it
with chicken and it was still quite good. I have impressed many with various
combinations of ingredients in the past. But I still haven't found the
combination I'm looking for in any repeatable way.

I have inherited my mother's recipe collection. It does contain a recipe for
Chicken Tetrazini as follows:

1. Mix -
    - 1 whole shredded chicken
    - 2 cans mushroom soup
    - 3 cups of broth or bullion
    - sherry
    - 1 lb spaghetti
2. Tap - with Parmesan cheese, lots.
3. Bake - 30 minutes.

My tetrazini is almost more of an Alfredo sauce made from a blend of cheeses,
butter, and other dairy. I have found mead made it tasty, even some beers work.
I've made with cream cheese, yogurt and rare cheeses. One of my favorite
attempts was with a cheese an Italian friend of mine had brought in from Italy.
I've found using buttered noodles as a base works nicely. I've experimented with
a variety of meats Pork is surprisingly good if a little dry.

Being that this quandary has eluded me for some time I present only options
here. In fact setting all this information down sets my mind to action coming up
with new combinations that might get the flavor I desire. I will now list a
recipe that I think will get as close as possible to the right flavor. While
maintaining a low price with available ingredients.

1. Bring a large pot of water to boil to make 1 lb spaghetti according to package directions
2. In a large saucepan, combine:
  - 6-8 chicken thighs
  - 1 bottle of beer
  - 1 bullion cube
  - water, enough to submerge the chicken at least 2 cups
3. Bring to boil. Let simmer at least a good 15 minutes. The longer the better.
4. Remove chicken. Separate meat from bones, skins, etc.
5. Add 2 cans of condensed cream of mushroom soup.
6. Add the meat.
7. Mix into spaghetti with 1 cup of Parmesan Cheese

When I get a chance to try this recipe I'll rate it. If it scores well it will
remain here alone. If I find a better recipe I'll shift this one down the page
and add a new best guess.

_Update (2004-10-05):_ I tried this recipe as written here and it is quite good.
It certainly captured the taste I was going for. It was a little soupy. Next
time maybe I'll try thickeners(corn starch), cooking the sauce uncovered or
letting it set-up for 10 min.

