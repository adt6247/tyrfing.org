Kutia or Kutya
==============
This pudding-like dessert is one of the oldest recipes. Probably, being 
made in one form or another into prehistory. Possibly, predating 
agriculture being made with collected dried seeds, grains and wild 
honey. As usual, I will try to update it, speed it up and make it a 
little more convenient.

I found this dish looking for traditional Christmas eve recipes. This is 
based off of an Ukrainian recipe I found. I modified it to be made with 
ingredients I could actually find in my super market. The big changes 
are barley instead of wheat. You can usually find barley with the dried 
beans. You could probably substitute any whole dried grain, like oats. 
Even rice would probably work, but then it would be more like rice 
pudding and you would loose that whole grain texture. The other big 
difference is the supermarket did not have bulk poppy seeds. So 
I substituted Solo canned poppy seed filling. This is already processed 
and is about half sugars, so I cut the amount of sugar or honey from 
the original recipe.

Ingredients
-----------
- 1 cup dried pearl white barley
- 6 cups water
- 1/2 cup canned poppy seed pie filling
- 1/4 cup chopped walnuts (or almonds)
- whipped cream

Procedure
---------
1. Soak barley in water overnight. I'd just use the pot I'm going 
   to make it in.
2. The next day, Bring the mixture to a boil.
3. Reduce to a simmer, stirring occasionally, until the barley has 
   exploded and the liquid has thickened. (1-4 hours)
4. Allow the porridge to cool.(~1/2 hour)
5. Mix in pie filling and nuts.
6. Refrigerate.
7. Serve chilled with whipped cream.

An immediate improvement that comes to mind is that this could be 
made less labor intensive by making the porridge in a crock-pot. I also 
wonder if the pie filling and nuts could be mixed into the warm 
porridge, removing another step. 

Some additional sweetness or pizazz 
could be in future versions. Being that this is a very classic dish it 
has a very subtle sweetness and simple flavor. The flavor could be 
nicely enhanced with a 1/4 teaspoon of vanilla, or a little cinnamon or 
nutmeg. If it isn't sweet enough your could add the entire can of pie 
filling or mix sugar into the porridge before it cools. If you wanted a 
more classic sweetener the original recipe uses honey, instead of 
sweetened pie filling, so that would add sweetness as well as a more 
authentic flavor. A popular flavor in these classic desserts is always 
dried fruit, such as raisins or apricots, which could add sweetness and 
a more complicated flavor as well.
