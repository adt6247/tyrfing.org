<h1>Chewy Chocolate Chip Cookies</h1>
<p>
This recipe is an attempt to create a cookie that stays soft without the weird flavors store brand soft batch cookies have. I tried to combine the chewiest 
factors of oatmeal cookies and chocolate chip. 
</p>
<p>
This is a combination of the Toll House and Quaker Oats cookie recipe. Then just a couple substitutions to make it more flexible,
</p>
<p>
Note: This recipe is vegan.
</p>

<h2>Ingredients</h2>
<ul>
<li>1.5c chocolate chips</li>
<li>1c flour</li>
<li>2c quick oats (old-fashioned will work)</li>
<li>0.5c white sugar</li>
<li>1c brown sugar</li>
<li>1t baking soda</li>
<li>0.5t salt</li>
<li>1t vanilla</li>
<li>0.5c apple sauce*</li>
<li>1c oil*</li>
</ul>
<h2>Procedure</h2>
<ol>
<li>Preheat oven to 375 degrees.</li>
<li>Mix dry ingredients together in a mixing bowl.</li>
<li>In a measuring cup combine the liquid ingredients.</li>
<li>Mix the liquid ingredients into the dry ingredients.</li>
<li>Drop tablespoons of the cookie dough onto a cookie sheet.</li>
<li>Bake for 10 min.</li>
</ol>

<p>
* The oil and applesauce are a primary knob on this recipe. Keep the total amount of liquid at 1.5 cups. As the amount of applesauce increases the cookies 
spread less and acquire more of a cake like consistency. More oil causes the cookies to spread more so that they get a thinner crispier consistency. 
</p>
