Anatomy of a Meatloaf
=====================
There are 4 parts of a meatloaf:

- meat
- loaf(starch)
- glue
- sauce

The meat adds tasty protein and grease to the dish. The starch was originally
added to extend the dish and while it still does that it is what gives meatloaf
its unique texture that makes it not a chopped steak. The Glue holds it all
together. the Sauce is the part that gives each persons meatloaf its own flavor.

The basic ratio is: 

- 1 lb Ground meat
- 1 cup crumbled starch
- 1 lg egg
- 1/4cup sauce

The Ground meat is usually beef, but could be any thing or a mix. Note when
adding sausage to a meatloaf never count it as more than 1/2 meat. The crumbled
starch is usually bread crumbs, but could be rice, cornmeal, potato flakes,
oatmeal. I prefer crushed matzo. The egg should be an egg. Note 1 lg egg is like
1.5 med eggs, but a little more or less hardly matters. The sauce is where
creativity comes into the recipe. you could go simple and just use ketchup. or
go for a classy flavor with Streak sauce. definitely pick something that
compliments your choice of meat and starch.

Form the loaves on a foil wrapped pan. Foil wrapping is really the only mildly
clean way to make meatloaf. I suppose you could just throw out the pan when you
are done, but that seems wasteful. Cooking the meatloaf may vary slightly based
on ingredients. It will vary more based on the thickness of the meatloaf. I
usually find that 375&deg; for 30-60 min does them all. I finished meatloaf will
be like a good hamburger dark but not burned on the outside and more importantly
not raw on the inside (a temperature of 165&deg; should do the job). I'd then
let it sit 5-10 minutes before slicing to cool.

Sample Meatloaves
-----------------
### My Meatloaf ###

I like to make this meatloaf, because the ingredients are cheap and it makes a very balanced classic meatloaf.

- 5(~1/4lb) 1 lb cheap burger patties(greasy meat make tasty meatloaf)
- 1 cup crushed plain store brand matzos
- 1 lg Egg
- 1/4 cup of combined:
  - onion chopped finely
  - ketchup
  - brown mustard

### Meatloaf Idea #1 ###

This meatloaf is inspired by the ingredients of stuffed peppers.

- 1lb ground beef
- 1cup cooked rice
- 1 lg egg
- sauce:
  - chopped bell peppers
  - canned crushed tomatoes

### Meatloaf Idea #2 ###

This meatloaf is a little more unusual. It goes for that Megalomart, Southern Country flavor.

- Meat: Chicken and/or Pork
- Loaf: cornmeal and/or Shake & Bake
- Sauce: BBQ
