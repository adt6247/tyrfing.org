Chocolate Wacky Cake
====================

This is my favorite cake recipe. I got this from a co-worker. I use it 
when ever I want to make a chocolate cake. I've used it for cup cakes. I 
like it because it is a from scratch cake recipe that is about as easy 
as a mix. 

This recipe has an interesting history originally dating back to egg 
rationing during WWII. This cake can easily be made vegan by using the 
water instead of the milk. Actually the original recipe I got said to 
use water, but I'm a big milk fan in baking. Actually the first time
I made this recipe they forgot to mention the water or milk so It was a 
little dry. Luckily a phone call saved the day there.

One key in this recipe is balancing the baking soda with the vinegar. 
Definitely error on the side of more vinegar. I first got this recipe as 
1 t Soda to 1 t Vinegar and to often the recipe was left with this weird 
bitter soda flavor. I've recently upped to double as present in this 
recipe, but I have seen online recipes that triple it to 1 T. The sour 
flavor of the vinegar is going to be more palatable than the bitter of 
the soda. 

I'm a big fan of dutch process cocoa. When I went to Switzerland, I got 
some Swiss cocoa there. That was better still. Choosing the right cocoa 
is the cornerstone to a good chocolate cake. This recipe actually has 
50% more cocoa than other recipes I've seen online. If you want to drop 
it down to 4 T. It should still be excellent. The cocoa I'm going to try 
next time is the Hershey's special dark label. It won't be the Swiss 
chocolate, but they stopped carrying anything labeled dutch process at 
my grocery store.

Ingredients
-----------
- 1.5c flour
- 1c sugar
- 1t baking soda
- 0.25t salt
- 6T cocoa
- 1c water or milk (+1/2c if making cupcakes)
- 5T oil (I prefer canola oil,mmm... Omega 3 fatty acids)
- 1t vanilla
- 2t vinegar

Procedure
---------
1. Preheat oven to 350 degrees.
2. Mix dry ingredients together into baking pan. I usually just run 
   them thru my sifter to help them combine. 
3. In a measuring cup combine the liquid ingredients.
4. Whisk the liquid ingredients into the dry ingredients. Make sure to 
   get the corners of the pan.
5. Quickly get the pan into the oven for 25-35min. The vinegar and 
   baking soda will start to react fairly quickly so you don't want this to 
   sit around or else it may not rise properly. 


Cupcakes
--------
1. Make sure to add the extra liquid.
2. The cupcake batter will need to be made in a bowl not directly 
   in a pan.
3. One batch should fill 12 cupcakes with 1/3 cup of batter.
4. Just as it was important to get the cake quickly into the 
5. oven, do not delay when measuring the batter into the cups.

Decorating this cake is pretty standard. You can let it cool and 
dust it with confectioner's sugar. Or frost it with store icing. 
Serve it warm with Ice cream. or mix a pkg of instant pudding with only 
1 cup of milk and pour that over the cake into the pan. Kind of a self 
leveling icing.

I have been considering a vanilla version of this cake. I would expect 
it to have a consistency not unlike pound cake. I would probably just 
leave out the cocoa and double the vanilla. We will see.
