Corned Beef
===========
Most of the "recipes" on this site are more of general guidelines. Suggestions
to help you experiment with tasty foods. This is not. This is a recipe I came
upon while experimenting with the simple joys of slow cookers. And trying my
hand at some classic St Patrick's Day fare. This dish nicely combines my search
for the perfect Germanic pot roast with my memories of Slavic potato and cabbage
dishes.

I started experimenting with this dish in college because it was cheap. Corned
beef brisket is probably the cheapest meat in the grocery store. Cabbage is even
cheaper around St Patrick's day they practically pay you to take it. And I've
never met anyone who considered potatoes a luxury food. This recipe probably
could serve 8. But with the crew around here it is more like 4.

## The Recipe ##
### Ingredients ###

- 1 corned beef brisket
- 1 head of cabbage
- 2 16oz cans of whole potatoes. or 8 potatoes cut into 2-3 large pieces each
- 1 bottle (or can) of beer

### Tools ###
- a Slow cooker (Crock Pot) - I have a nice big one that's designed to fit a whole chicken, but a smaller one will do.
- a knife and somewhere to use it.

### Procedure ###

1. Cut the cabbage into 4 large wedges and remove the stem. This should be done
   in the same way as a wedge salad. Smaller crock pots may require the wedges to
   be cut in half again.

2. Place the cabbage wedges into the bottom of the slow-cooker. Do this
   carefully. If the wedges are kept tight now they will survive the cooking
   process. Allowing them to be served whole. Generally considered a nice
   presentation.

3. Add the layer of potatoes on top of the cabbage.

4. Lay the corned beef on top. Fatty side up. Now is a good time to make sure
   you lid is going to fit on. If it does not, just pull out some potatoes or maybe
   a wedge of cabbage. If your coned beef came with a little season packet, put it
   in underneath the corned beef.

5. Pour the beer over top. and put the lid on. I usually try to get to this step
   the night before then I toss the removable crock from my slow cooker in the
   fridge overnight, but It works just as well all done at once.

6. Set your slow cooker to it's low setting.

7. Wait at least 8 hours. I usually pass this time by going to work, but I used
   to pass this time by going to class. once I even passed this time by doing work
   around the house. Be careful though if you or someone you know is around the
   slow cooker. DON'T OPEN IT. No matter how good it smells or how curious you get,
   don't open it. That just slows the cooking down and prevents even cooking. Rumor
   has it that 4 hours on high is just as good, but that doesn't really seem like
   _slow_ cooking to me.

8. Remove the corned beef, carefully, when you've waited long enough. It's been
   though an ordeal it will fall apart at a whim so be careful. It needs to rest
   for 5 min. Most pros would say wrap it in foil. I usually just let it sit on a
   plate in the microwave while it is off. While the meat is resting I usually
   leave the lid off the slow cooker so that it can cool a little.

9. Thinly slice the meat across the grain.

10. Serve with a carefully removed wedge of cabbage and a couple of potatoes. I
   like it with a little rye bread and butter.

## Notes ##

Currently I just throw out the liquid left in the slow cooker. My instincts tell
me though that there is sauce potential there. I just haven't found it yet.

I hope you enjoy this recipe. Or, at least found it informative. Good Luck.
