Cola Beverage
=============
This is the newest iteration of this recipe the history is preserved  below.
This time I started with the recipe on wikipedia. Used a spreadsheet to
normalize the ingredient ratios up to a 2L container. I assumed the lemon juice
was 5% acid and the spices were 1% essential oils. I also cut back the sugar to
be more similar to  Kool-aid levels being that I made it without carbonation. It
was pretty good. It's a very tasty beverage, and can make you think of  cola

Ingredients
-----------
- 8-12 T(120-180 mL) sugar
- 1/2 t(2.5 mL) nutmeg
- 1/8 t(0.6 mL) coriander
- 1/2 + 1/8 t(3.1 mL) cinnamon or cassia
- 1 T(15 mL) lemon juice
- Coloring
	- ~1/4 t caramel coloring
<br />-or-
	- ~1/4 t black food coloring
<br />-or-
	- ~1/8 t red food coloring
	- ~1/8 t blue food coloring
	- ~1/8 t yellow food coloring
- seltzer or water

Procedure
---------
1. In a 2 Quart or 2 Liter container, Mix sugar and spices dry thoroughly to
   start the flavor distribution.
2. Add the lemon juice and coloring.
3. Add enough water to fill ~1/6 of the container.
4. Mix into a syrup.
5. Fill container with seltzer or water.
6. Serve over ice.

If you want cola syrup you can stop once you have the container 1/6th full.
This recipe should create a 1/3 L of syrup. which at a 5:1 ratio is how much
syrup it would take to make 2 L of soda.

I thought about using baking soda to carbonate it. I actually put some baking
soda in a cup, added some cola, and some extra lemon juice. It kinda worked, but
is far from a final plan.

I've also considered making it into a tea recipe for caffeine content. To the
above recipe you would want to soak about 4 teabags in the mix. I  would be
curious to see what kind of tea you got if you made this hot  without sugar or
coloring. Really just targeting the tea and spices. It should be like a cola
tea. This could probably be easily simulated with using ice-tea mix instead of
some of the sugar. if you think iced coffee is more the way to go. Coffee has
about twice the caffeine so the 2 Liters should only need about 2 cups worth of
coffee.

Original article (archived 2010-07-06)
--------------------------------------

This recipe is based on the formula for 
<a href="http://en.wikipedia.org/wiki/OpenCola_(drink)">Open Cola.</a>

I saw the formula and my mind started to turn. I thought there is 
something that could be simplified and made with more common 
ingredients. So I substituted spices for the spice oils, lemon juice for 
the citric acid and citrus oils. And I had a bottle of black food 
coloring lying around so that made it in. In the experimental phase I 
found making it black was very key to the mental aspect of the flavor. 
It tasted much more correct when I added the food coloring.

Ingredients
-----------
- 1 t(5 mL) nutmeg
- 1 t(5 mL) cinnamon or cassia
- 1/4 t(1.2 mL) coriander
- 12 T(180 mL) sugar
- 4 T(60 mL) lemon juice
- black food coloring
- 5 c(1200 mL) seltzer or water

Procedure
---------
1. Mix sugar and spices dry thoroughly in solid container to start the flavor
   distribution.
2. Add the lemon juice and food coloring.
3. Mix into a syrup. You can add a little warm water, if you feel the need.
4. Mix into seltzer and ice.

I currently have a x1.5 size recipe in a 2L bottle in my fridge to see if the
flavors blend more overnight.

The spices are in the correct internal ratio, which I think becomes obvious if
you smell the sugar spice mixture, I think they may all need to be cut
proportionally.

The acidity seems correct. It is really sweet, but that is the way it is with
sodas. Cutting the sugar down to as much as 1/4 would give you a milder
version. Particularly if you use non-carbonated water. Most sodas are so sweet
partially to over come the taste of soda water.

The syrup step might benefit from actually being heated to make a syrup. Then
cooled before adding to the seltzer.

The black food coloring I have seems to be made from a mixture of red, blue and
yellow. I'm also thinking about the classic caramel coloring which should be
able to be make by caramelizing some of the sugar before making the syrup.
