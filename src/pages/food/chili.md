A Crock of Chili
================
For many people Chili is a very specific dish. For me it is more like a wide
variety of dishes. It is a wide variety of stews that contain beans, tomatoes
and heat (spicy heat that is). As a matter of fact, I would say that most stew
recipes could be turned into chili by adding beans, tomatoes and hot sauce. You
would probably want to remove really starchy ingredients that might conflict
with the heat like potatoes, pasta or barley.

Chili is one of the best leftover dishes. Almost anything can be used as an
ingredient in chili.
