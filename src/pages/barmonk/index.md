# Bar Monkey Project

1. ## Introduction ##
  1. This site is dedicated to our experimentation, designs and implementation 
     of our own bar monkey.
  2. What is a bar monkey you may ask. It is an automated mixed drink vending
     robot. Many people have worked on this area before. Our goal was to make 
     a more sophisticated system with a simplified design.
  3. The design was broken up between us based on our areas of expertise. and 
     seperate reproducable subcomponents.
  4. For our initial revision we decided upon 16 different ingredient paths for 
     ~200 different drinks. We also decided against the pumps common in most 
     other designs as solenoid valves and pnuematic systemes are still required. 
     Instead we went with a constant pressure system with solenoid valves. This 
     cut cost and complexity.

2. ## Pnuematics ##
  1. 5 lb CO<sub>2</sub> canister
  2. Pressure Regulator
  3. Gas Manifold

3. ## Hydralics ##
  1. 2 Liter Canisters
  2. Bulk head quick connect fittings
  3. Solenoid Valves
  4. Flow Rate Calculations

4. ## Electronics ##
  1. The solenoids that were selected were rated for running at 24VDC at X watts. 
     I had an old power supply that met the requirements and decided to use that. 
     They also were default closed so if the machine loses power it won't be 
     pooring drinks.
  2. The controller circuit that was selected was the  
     [0/16/16 Phidget](http://www.phidgets.com/products.php?category=0&amp;product_id=1012). 
     It was rated for 30V, had Debian GNU/Linux libraries and 16 outputs.
  3. At the Trenton Computer Festival, I came upon a used Fujitsu Stylistic LT
     C-500 for only $75. It has a touchscreen, battery backup, wireless IR
     keyboard, onborad sound  and a docking station with networking and power
     connectors. It was a donation so it had its harddrive wiped clean no OS and
     now drives from which to install, but I was able to find an online 
     [site](http://www.neurath.org/stylistic_lt_c500/) describing how 
     to install a basic Debian GNU/Linux system.
  4. The wiring was fairly straight forward. The phidget instructions make a good
     reference. I wired the negative terminal from my power supply to one of the
     common ground terminals on the phidget. I then wired one lead from each
     solenoid to a common Positive source from the powersupply. I then wired each
     of the solenoids remaining leads to one of the output terminals on the
     phidget. Now when the phidget output it turned on, the solenoid completes
     it's connection to ground and is able to switch on.
  5. Note: The phidgets have overcurrent protection to buffer them against  the
     direct connection of an inductor. Other control circuits may need additional
     buffering if the teminal connects directly to a transitor or else extreme
     voltage spikes could result.  It seems unlikely that a control circuit without
     an external power supply could drive a solenoid valve. If you get solenoids
     designed for AC or high voltage, you may want to consider getting more
     isloation by using one of the relay models, these will have a lower switching
     speed though.

5. ## Software ##  
  1. A wealth of software went into building up the controller, fortunately we
     were able to largely leverage Open Sorce Software to speed up development.
     I  will put the information on installing a functioning Debian/GNU Linux
     environment on the Fujitsu Stylistic LT C-500, 
     [here]({base-url}comp/fuj_sty_lt_c500.html). Once we had a working base
     Debian system we made sure we had  the following packages: libphidgets-dev,
     python-wxgtk2.8, python-numpty. We were running with the LXDE desktop
     environment and also installed a music player (gmpc/mpd) so that the
     machine  could function as a jukebox while dishing out drinks.
  2. On top of this base I created a C based command line utility that would
     connect to the phidget and cycle thru each output turning it on for a
     certain  period of time. This command <code>barmonk</code> takes the
     Phidget USB ID (0x44) and the serial number as its first parameters to
     know which phidget to use. The remaining parameters are a list of how long
     to turn on each output in  microseconds. If you get errors, make sure you
     are in the <code>phidgets</code> group and check this 
     [bug report](http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=338750)
  3. A wxPython Graphical User Interface, <code>wxbarmonkgui</code>, was the
     next piece of software. It parses the list of drinks, ingredients and
     controllers to create a menu for the user to select which drink to make.
     The drinks.xml file is the configuration database of the entire control
     software. New drinks can be added for personal preference. Ingredient
     properties can be changed, including the ever important multiplier field
     which is how long the ingredient must be on in microseconds to get 1 ounce
     (~30 ml). The shot drinks in the menu should pour that one multiplier
     amount. We use them and a graduated cylinder to tune the system. For 20
     PSI and 1/16" opening our times should be pretty close or use 1000000
     for new ingredients. The  controller section  needs to be set to use the
     correct serial number of the phidget. The system could be upgraded to 32
     ingredients just by adding a second controller, 16 more ingredients and
     the drinks to use  them to the xml. Drink groups can be created and drinks
     moved between groups and favorties with the xml as well. In order to fill
     the dead space I chose a quite nice image from the Debian version of the
     [openclipart](http://www.openclipart.org/) package. Of course,
     feel  free to replace it with anything you'd rather look at.  
  4. We also created a cgi-bin that would allow other frontends, such as a flash 
     interface in the works, to contact the backend and get a drink poured.
  5. **Software Releases:**
     1. [barmonk_20090703_release.tar.gz]({base-url}/barmonk/barmonk_20090703_release.tar.gz)

6. ## Visuals ##
  1. There is a more CPU/Memory intensive flash interface in the works. It has 
     on screen tutorials, dancing monkeys and a more pleasing full screen mode.

7. ## Wrap Up ##
  1. If you have questions, feel free to drop us a 
     [line](mailto:tkotz@tyrfing.org).
  2. We will try to post answers to questions on this site. As well as update  
    as we get more chance to add content/new releases.  So keep your eyes open.

