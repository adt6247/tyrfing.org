Welcome to Tyrfing - Ted Kotz's Bookmarks and Home Page
=======================================================

Why tyrfing.org?
----------------
I get many questions about _why tyrfing.org?_

The short answer is simple: _it's cool._

The long answer is the machines on my home network are named after types of
swords. So I thought a famous mythical sword would be a good domain name.
excaliber.* was taken and was a little to commercial. The mythical Norse sword
tyrfing immediately came to my mind. Tyr being the Norse god of swordsmanship.
Fing meaning fang, a common way to refer to one's blade. So Tyrfing was Tyr's
Fang. The sword of the perfect swordsman. It seemed fitting. To find out more
than you want to know about this subject, try 
[Wikipedia.](http://en.wikipedia.org/wiki/Tyrfing)

Hobby How-Tos
-------------
I figure this information may be useful to someone out there. It's at least
useful to me, and this keeps it all centralized. Some of these are how-tos. Some
of these are just lists of technical info. Some are discertations on the nature
of the feild of study. And some are just stories of my own humiliating
misadventures. I hope you can learn something from them.

**DISCLAIMER:** Life is a gamble. Following any of these guides may not lead
you to the same results. In some cases, worse yet they may lead you to the same
results. If in doubt consult a professional.

- ### Computers &amp; Networking ###
    - [Operation: Bar Monkey]({base-url}barmonk)
    - [The Perfect Debian Gnu/Linux Etch Desktop]({base-url}comp/debian-etch)
    - [Live CD based router]({base-url}comp/live-router)
    - [Android Rocks]({base-url}comp/android)
    - Favorite Debian Packages
    - How-to write C/C++/Java the tyrfing.org way
  
- ### Recipes (or Food How-tos) ###
  - [Chewy All-Oatmeal Cookies]({base-url}food/oatmeal-cookies)
  - [Chili]({base-url}food/chili)
  - [Meatloaf]({base-url}food/meatloaf)
  - [Tetrazini]({base-url}food/tetrazini)
  - [Corned Beef, Potatoes &amp; Cabbage]({base-url}food/corned-beef)
  - [Fruit Crisp]({base-url}food/crisp)
  - [Instant Chocolate Oatmeal]({base-url}food/oatmeal)
  - [Kutia or Kutya]({base-url}food/kutia)
  - [Cola Beverage]({base-url}food/cola)
  - [Wacky Chocolate Cake]({base-url}food/cake)
  - [Old Chewy Chocolate Chip Cookies]({base-url}food/chewy-cookies)
  - [Pierogi]({base-url}food/pierogi)

- ### Home Improvement ###
  - [Best Toolbox contents]({base-url}home/toolbox)
  - [Exterior Door Install (or Just how bad can a lock install go)]({base-url}home/door)
  - Extending a brick foundation
  - Resheathing a quasi-exterior room
  - Quick guide to House wiring

- ### Role Playing Games ###
  - [Dungeons and Dragons - Second edition and d20]({base-url}rpg/dd)
  - [Star Wars - d20]({base-url}rpg/starwars)
  - [Paladium(not Rifts)]({base-url}rpg/pal)
  - [Rifts]({base-url}rpg/pal/rifts)
  - [Cyberpunk]({base-url}rpg/cyber)
  - [CHI - A system of my design for KungFu type action]({base-url}rpg/chi)
  - [My System - I never named this system, but it looks good and balanced]({base-url}rpg/system/html)
  - [Character Sheet]({base-url}rpg/system/charsheet.pdf)

Who am I?
---------
Many people who have read this far (I'm pretty impressed), are probably
wondering who I am. I imagine your thinking, _"This guy just rants on and on.
Who does he think he is anyway?"_

My name is [Ted Kotz](mailto:tkotz@tyrfing.org). I setup this page
because I had the domain for networking reasons and my brother wanted to setup
some web space. So I figured we'd split the cost on a VPS. Then I just sort of
threw together some info I had lieing around and called it a Website. Maybe with
time it will also gain order.

I graduated with a BS in Computer Science with a minor in Computer Engineering
from NJIT in 2000. I'm an Embedded Systems Programmer for ITT Industries. I live
in Newark, NJ with my brothers and housemates. I recently bought the house we
live in and spend a good portion of my free time trying to fix it up.

I'm a practicing Roman Catholic. Meaning I go to church every Sunday and on holy
days. I'm a fairly moderate Catholic. Meaning I probaly won't burn you at the
stake for not. Feel free to hold this against me, suffering is good for the
soul.

When I get around to setting up a mail server, there will be link around here to
contact me. And now the what you've been waiting for:

- [Ted Porn (Link Down)](http://localhost/).

## Links ##
- ### Friends &amp; Family ###
  - [Rob Kotz](http://www.robkotz.com/)
  - [Martin Maranski](https://plus.google.com/114797380052316738349)
  - [Cleo Bertrand](http://bansheeproductions.net/)
  - [Aaron Traas](http://www.traas.org/)
  
- ### Pages Everyone Should Have Bookmarked (I know I do) ###
  - [Bookmarks]({base-url}bookmark.html)

