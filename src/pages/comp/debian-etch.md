Tyrfing's Perfect Debian Gnu/Linux Etch Desktop
===============================================
What is this?
-------------
This is the description of how to run you base Etch install so that you end up
with a very functional KDE Desktop. I see a lot of hype out there about Ubuntu.
I've been using Debian as a desktop OS for years (since Potato). I figured all
ubuntu really has is a couple non-free packages in there standard install and
some standard settings. I figured I could get together a setup that would be at
least as good plus have the power of the full Debian repository. Etch made this
easier with its new desktop tasks. This install keeps you intimately involved in
the whole process so that you get what you want and can see how it works.

Why Debian?
-----------
I came to Debian by way of Red Hat, Mandrake, and Corel Linux. They were
terrible. Package management was painful, to this day when I see RHEL it think
Rpm dependency HELl. I don't know what they were thinking when they changed to
such a terror inspiring acronym. Configuration management was a beast. linuxconf
often disagreed with the way the packages wanted to be configured. If you
editted the configuration manually some "convenience" tool would usually destroy
your work. Upgrading was a nightmare. If I had a nickel for each time it failed
me. I'd probably have 15 cents, but after three required reinstalls even I learn
my lesson. This taught me to set up my system right at install time and never
modify it again. Not exactly fun for a guy who likes to tinker.  After this
mess, Debian was a thing of beauty. it just worked. Thinking back to the first
time I used apt to install a new piece of software almost makes me tear up now.
Each package is lovingly prepared to stand alone and integrate with the system.
dpkg asks configuration questions to guide you thru basic setup, but in the end
the configuration file that the software developers designed to configure the
software is the default means of configuration. They do an excellent job listing
the options and commenting there settings. I could go on, but I think on to the
next topic. Note: kernel-package rocks

Why KDE?
--------
When I was but a Linux pup, everyones' view of KDE was "yea it's a lot easier,
but look at that license on QT." Then it went GPL. But it was too late most
distros already decided on Gnome as default. Gtk has a lot of nice features.
LGPL is a better license for a library. The C based executable styles is
brillant. I actually use this feature because you can have gtk apps call QT
widgets as there style. I think C is a poor choice of language for widget
programming as the Object orientted programming paradigm is just so well suited
to it. I actually like the WxWidgets toolkit myself (More QT style object
oriented application programming toolkit plus Gtk license and technical
benefits). KDE has better integration than Gnome. the parts seem to work better
together. The big win though is configuration KDE control center and various
settings windows present options that are actually extensive and useful. Gnome
seems to limit GUI configuration to a handful of mostly useless options, and
anything really useful is in there XML config files. A quick note about XML
config files. They suck. Clarification: they suck to edit by hand. XML is great
for marking up text. or keeping a interprocess protocol "human readable" yet
easily parsable. If you are going to write a full featured configuration editor
that saves the data in XML that's fine, but If you need to edit the
configuration file in order to use a standard feature it is terrible. when you
have a simple editor with the config file open just a list of parameter value
pairs is the way to go. Easy to read. Easily commented. Relatively easily
parsed.

What's the procedure?
---------------------
You need to acquire a Debian install CD. Any version should work. I recommend
net install or business card as we won't be using the packages on the disks
possibly anyway. Remember choose install options with confidence. If you mess
up, you just need to restart. It usually takes a couple tries to get the system
how you want it, if you haven't done it before.

If you are manually partitioning, I like creating a swap partion on each disk
totaling 2GB. Even though you shouldn't need it. Harddrive space is cheap. You
can get some of this space back by mounting /tmp on a tmpfs filesystem.

My biggest disagreement with the default setup of Debian is the fact that it
doesn't by default send its hostname when it configures the network by DHCP. I
like this because it allows my DHCP/DNS server to keep the hostnames and ip
addresses synchronized, without my intervention. To get the option to send the
hostname to the DHCP server either:

Cancel DHCP network init.

   \- or -

If you miss it, when asked for your hostname select go back.

When it comes to selecting which software task remove all tasks. We will select
them manually later.

After reboot, login as root. The first thing we want to do is edit our package
sources to include contrib and non-free. These group names are just a reference
to the license on the source code. Just edit /etc/apt/sources.list. I prefer
nano but feel free to use whatever editor you want (vi and ed should be
installed).

~~~
#nano /etc/apt/sources.list
~~~

I usually comment out (precede with a #) or remove any line for the CD or deb-
src lines. Where _main_ appears in the _deb_ lines add _contrib_ and _non-free_.
Such that:

~~~
deb http://ftp.xx.debian.org/debian etch main
deb-src http://ftp.xx.debian.org/debian etch main
~~~

Would become:

~~~
deb http://ftp.xx.debian.org/debian etch main contrib non-free
#deb-src http://ftp.xx.debian.org/debian etch main
~~~

If you are upgrading from an older version of debian, you can just change the
sources.list file to point  to _etch_ rather than the previously installed
version (sid, stable, woody, sarge, etc). A reasonable  default sources.list
file might look like (with the country code  properly replaced):

~~~
deb http://ftp.(country code).debian.org/debian etch main contrib non-free
deb http://security.debian.org/debian etch/updates main contrib non-free
~~~

After saving this file and exiting (Ctrl-X does both in nano), update the list
of package to include the repository changes that we made.

~~~
aptitude update
~~~

Now we install the first package to make sure that everything worked. I usually
use localepurge. It doesn't need many dependencies and the earlier you install
it the better. The localepurge package will cause package installs to delete
unused locale data, this could same a reasonable amount of space on a machine
that other wise would just be used up by data in languauges you don't understand
or use. If you are reading this howto then you probably want en and en_US.UTF-8
to still be generated. Make certain they are on the list. It should default to
the locale you chose at install, but it doesn't hurt to make sure.

~~~
aptitude install localepurge
~~~

Next we update the rest of the packages in the base install to make sure they
are up to date. In the intial release version of etch there was a kernel bug.
This will be updated at this time.

~~~
aptitude dist-upgrade
~~~

Next we reboot so the new kernel can be loaded. This isn't needed if the kernel
version didn't change, but it doesn't hurt, either way.

~~~
reboot
~~~

If you rebooted, log back in as root. Now we can start to install the bulk of
the packages. We do this by going in to aptitude's interactive mode.

~~~
aptitude
~~~

The interactive aptitude command list's most import commands are:


### Aptitude commands ###

arrows
: Move selection around package list

enter
: Expand/collapse tree view

/
: Search for package(When searching I prefer to change to the Flat package list view</dd>

?
: Help

+
: Mark package for install

-
: Mark package for uninstall

g
: Go downloads, installs, removes any packages that have changed state

First go thru the tree to the list of task add all of the following tasks. If
they are expanded when you select the task you should see all of the sub
packages become selected. Then the contained packages that aren't  really wanted
can be removed.

### Tasks ###
- Print-Server
- File-Server (disable netatalk)
- Desktop
- KDE Desktop
- Laptop (disable bluetooth)

Now that we have the high level task selected, choose a list of individual
package we  want. I usually first goto the menu (Ctrl-T), and change the view to
flat package list. Then I search for each package I want in alphbetical order. I
install most of these packages below plus any hardware specific ones that are
needed.

### Packages ###
- alsa-oss
- bash-completions
- build-essentials
- cron-apt
- flashplugin-nonfree
- gaim
- gstreamer0.10-fluendo-mp3
- gstreamer0.10-pitfdll
- gstreamer0.10-plugins-bad
- gtk-qt-engine
- less
- localepurge
- lsb
- menu-xdg
- mesa-utils
- mozilla-mplayer
- mozplugger
- ntp
- ntpdate
- openssh-server
- pmount
- prelink
- preload
- sun-java5-plugin
- sysfsutils
- sysv-rc-conf
- ttf-freefont
- ttf-opensymbol
- ttf-thryomanes
- totem-gstreamer

### Common Hardware Specific Packages ###
- nvidia-glx[-legacy]
- nvidia-kernel[-legacy]-2.6-version (select kernel first)
- libglide3 (for tdfx cards)

Once all the packages are select hit g to go. Aptitude will download and install
all the selected packages. During the install aptitude will ask questions about
the packages as it installs them usually just accept the default.

I usually select to update WINS settings based DHCP

Once all the packages are installed exit aptitude (q will back you out). Next we
can look at some  of the things aptitude does for us. Look how much space can be
freed by removing old packages and  unused config data.

~~~
df

aptitude clean

aptitude purge ~c

df
~~~

Now that we verified some basics of the install lets configure some of the new
packages.

Uncomment the block about bash completions in bash.bashrc

~~~
nano /etc/bash.bashrc
~~~

Prelink needs to be enabled after it is installed. You can do this by editing
the configuration  file /etc/default/prelink. Enable prelinking by setting
PRELINKING to yes.

~~~
nano /etc/default/prelink (set PRELINKING to yes)
~~~

from:

~~~
PRELINKING=unknown
~~~

to:

~~~
PRELINKING=yes
~~~

Next we need to verify our X configuration.

~~~
less /etc/X11/xorg.conf
~~~

The X server configuration can be changed either by reconfiguring the package:

~~~
dpkg-reconfigure xserver-xorg
~~~

or by directly editing the configuration file

~~~
nano /etc/X11/xorg.conf
~~~

Next make certain that audio is properly configured. Running alsamixer can
verify that the HW was detected and setup good levels.

~~~
alsamixer
~~~

After the levels are set, save them away so that the will be restored on reboot.

~~~
alsactl store
~~~

If there is a problem with the audio device, you can run alsaconf to attempt
guided sound detection.

~~~
alsaconf
~~~

You should now be ready to start X(/etc/init.d/kdm restart), if you want. It
might be easier to just reboot though.

~~~
reboot
~~~

If everything worked you should see the graphical login screen. If not, back to
X config.

Login. The following KDE setup may be a little light on flash, but give good
performance even on older equipment. During the initial login wizard, when you
get to the eyecandy-o-meter, slide it down to the fewest effect. Then  expand
details and enable Smoothed fonts.

After the login completes, you will want to get familiar with the Control Center
in the K start menu. Expand Sound & Multimedia. Select Sound System. Under Auto-
Suspend, set the idle timeout to 0 secounds. This prevents the KDE sound system
from colliding with other applications. If sound still acts wonky set Enable
sound system to disabled. Changes to the control center should be commited or
cancelled before changing where you are in the tree. It will remind you of this
if you try to click away.

The Control Center also let you edit some system settings. Under System
Administration, select Login Manager. Hit the Administration mode button and it
should ask for your root password. This lets you change configuration that as a
user you wouldn't have permission to. Under the shutdown tab set your boot
manager to whatever you are using. This is probably Grub if you followed the
normal install. If you don't know you can just look at the next time you reboot.
Leaving it on none works, but we want more than just works.

If you are planning on using the 3d openGL accelleration, you should test it out
first.

~~~
glxinfo
~~~

This will display the supported openGL features. The key is to look for the line
towards the top that says Direct Rendering. It should say Yes, meaning that the
graphics hardware is doing the work. you can do a simple test of performance
using:

~~~
glxgears -printfps
~~~

That covers the basic install. Feel free to experiment. You got it running once,
you can do it again. Using aptitude you can search or browse for packages that
contains software that might interest you.

What are some fun debian game packages?
---------------------------------------
Everone likes a good game. Here are a couple voted popular over at 
<a href="http://www.happypenguin.org/">Linux Game Tome</a> that I like as well.

### Games ###
- airstrike
- fillets-ng
- freeciv
- frozen-bubble
- lbreakout2
- lincity-ng
- neverball
- pingus
- planetpeguin-racer
- planetpeguin-racer-extras
- pysol
- scorched3d
- sopwith
- supertux
- supertuxkart
- tremulous
- tuxkart
- wesnoth
- wormux
- xfrisk
- xmoto
- xscorch

Other packages of interest
--------------------------
### Packages ###
- boinc
- splashy
- gnash
- compiz
- qcad
- scite
- apt-listbugs
- gtk-engines-qtpixmap(only if you use older gtk1 apps)
- gstreamer0.10-plugins-really-bad
- gstreamer0.10-ffmpeg-full
- w32codecs
- ttf-inconsolata
- cron-apt
- glest
- tmw
- bos
- stratagus-gl
- chntpw
