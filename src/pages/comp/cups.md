This needs to be approached as two separate tasks first make sure 
printing is working on the linux box. Then worry about getting Samba to 
share it with windows.

Setting up the printer.
1. Make sure you have all the printer packages you need installed on 
linuxA. 
 
aptitude install cupsys cupsys-bsd hpijs

2. make sure cupsd.conf is properly configured. Your settings seem fine.

3. restart cups

/etc/init.d/cupsys restart

4. web browse to http://localhost:631/

5. you should see a web interface, with a printers tab. click the 
printer tab.
It might ask for you're root username and password.

6. click add a printer and follow the wizard. Print a testpage if you 
like.

7. after adding the printer, run lpq to see that the printer is listed.

lpq

8. you should now be able to print.

lpr testfile.txt

Setting up samba
1. Install all needed packages.

aptitude install samba swat

2. web browse to http://localhost:901/

3. you should see the swat web interface for configuring samba. samba 
configuration depends heavily on how you plan to use it. swat makes it 
much easier.

4. you should make sure in the printer tab it is set to use cups. 

5. restart all in the status tab. 

6. if you go back to the printers tab it should list your printer.

7. windows machines should now be able to see it.

good luck
