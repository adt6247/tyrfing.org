# Using Debian's live-package to Build a Live CD Based Router

I have a router, not some specialty gadget. It's a PC with 3 interfaces. One
10baseT card that connect to my ADSL modem. One 100baseT card that connects to
my home network switch switch. And one Prism54 based 802.11g card. It runs
Debian Gnu/Linux. And it happily routes away. My biggest problem with it has
always been the Hardrive. It doesn't need lots of harddrive space or really fast
access so I tend to just stick whatever old HD I have lying around in there.
This has lead to a couple problems. These old harddrives tend to be flaky, so I
spend a good portion of time rebuilding this machine due to harddrive failures.
After the first couple I started keeping CD backups just so I could copy the
configuration out of <code>/etc</code>. More recently I've run out of old
hardrives to put into it. Faced with dropping enough cash on a small reliable HD
as it would to buy a WRT54GL. I thought I have plenty of old cdrom drives, could
I make a live CD that would provided me with the comfort and flexibility that I
have come to expect from my HD based Debian system?

Searching around revealed that the key tools I needed were already available
from the live-package package available in etch (and I assume later versions).
Its key tool is the [make-live][1] program which is far more designed to make
live CDs for use in portable desktop demos than it is for making very specific
complex networking configs. But with some config file modifications and chroot
hooks it could be made to work.

## A little on make-live

If you just run: 

    make-live
    

It will pull together a default set of packages into sub-directories of
<code>./debian-live/</code> and create <code>./debian-live/binary.iso</code>. 
I would recommend running it as:

    make-live --hook bash
    

It will kick off a bash shell as root in the chroot. This way you can configure
anything you want before it makes the final iso image.

If you want to rebuild the iso image after changing a file in 
<code>debian-live/chroot</code> run: 

    rm debian-live/.stage/image_binary
    make-live
    

If you want to re-enter a shell in the chroot and then rebuild the image: 

    rm debian-live/.stage/image_binary debian-live/.stage/chroot
    make-live --hook bash
    

Here are the key changes I made to <code>/etc/make-live.conf</code>. 

    # /etc/make-live.conf - configuration file for make-live(8)
    
    # make-live does work out of the box. However, if you want to customize it,
    # you can edit the variables below. Look make-live.conf(8) for more information.
    
    # Bootstrap architecture    (Default: auto-detected)
    LIVE_ARCHITECTURE="i386"
    # specify the architecture of your target platform rather than using that of
    # your source example my router is i386 my desktop is amd64
    
    # Boot parameters       (Default: empty)
    LIVE_BOOTAPPEND="ip=frommedia hostname=XXXXX"
    # tell casper to not overwrite the /etc/network/interfaces file
    # specify what to set the hostname to
    
    # Debian distribution       (Default: sid)
    LIVE_DISTRIBUTION="etch"
    # I don't want a router based on unstable. Do you?
    
    # Kernel flavour        (Default: auto-detected)
    LIVE_KERNEL="686"
    # just like LIVE_ARCHITECHTURE specify what the target needs.
    # I think you can also use the LIVE_KERNEL_PACKAGES option to
    # specify a personelized kernel image
    
    # Debian mirror         (Default: http://ftp.debian.org/debian/)
    LIVE_MIRROR="http://apt-proxy:9999/debian/"
    # I use my local mirror
    
    # Debian security mirror    (Default: http://security.debian.org/)
    LIVE_MIRROR_SECURITY="http://apt-proxy:9999/debian-security/"
    # same here
    
    # Packages          (Default: empty)
    LIVE_PACKAGES="pppoe pppoeconf wireless-tools dhcp3-server bind9 iptraf 
            popularity-contest iptables libc6-i686 udev ssh less nano vim cupsys 
            cupsys-bsd cupsys-client syslinux lynx lynx-cur mutt dnsutils 
            traceroute tcptraceroute mtr-tiny chkrootkit vrms openvpn ntp lsb"
    # packages that my router needs feel free to make your own adjustments
    
    # Image templates       (Default: /usr/share/make-live/templates)
    #LIVE_TEMPLATES="http://www.tyrfing.org/usr/share/make-live/templates"
    # note this setting if you want to change anything in the boot image it needs
    # to be made in this dir
    

## Isolinux

When the CD boots it uses [isolinux][2] to bootstrap linux. The only change I
felt it needed was I wanted it to autoboot after 2s, not to keep waiting for me
to hit enter. All that had to be done was in 
<code>/usr/share/make-live/templates/syslinux/isolinux.cfg</code>
(note relationship to LIVE_TEMPLATES above) the line:

    timeout 0
    

was changed to: 

    timeout 20
    

This file contains many options that could be tweaked. also the template
directory gives you the ability to change the files that are available to
casper.

## Casper

The Live CD system is actually run in the [casper][3] framework. It is designed
to allow a bootable system from a read only media. It does this by merging read
only file system images with ram disks to create a writable virtual filesystem.

The key configuration changes for casper were passed in as boot parameters in
the <code>make-live.conf</code> file. By default, it will set you hostname to 
<code>live</code> and configure all you interfaces by DHCP.

## Files of import

Here is a list of all the key configuration files I changed in the chroot before creating the iso image. 

*   <code>/etc/network/interfaces</code> - specify how to set-up the interfaces
*   <code>/etc/iptables</code> - The file I usually store my default iptables settings
*   <code>/etc/udev/rules.d/z25_persistent-net.rules</code> - keep interfaces matched to names
*   <code>/etc/dhcp3/dhcpd.conf</code> - specify zones and network info, [linked][4] to bind9 
*   <code>/etc/bind/</code> - setup local DNS caching, [linked][5] to dhcp3.
*   <code>/etc/ppp/</code> - setup for pppoe connections

## Other Tools

I found qemu very useful in testing images before I burned them. 

    qemu -m 256 -cdrom debian-live/binary.iso -boot d
    

The etch replacement for cdrecord is wodim to burn this to a cd I used: 

    wodim -v debian-live/binary.iso
    

Most of the defaults worked well for me. While I was trying out multiple different images I used a CDRW that can be burned as follows. 

    wodim -v blank=fast debian-live/binary.iso
    

## Known Issues

1.  I couldn't use dpkg, aptitude, etc to configure packages. They always installed with default settings

## Conclusion

This should give you a better understanding of the basic tools availble to make a live CD. I will probably expand this with more detail later. If you know your way around a Debian system you should be able to create a custom disk. And then you too can say good by to magnetic media in your router. 

 [1]: http://debian-live.alioth.debian.org/
 [2]: http://syslinux.zytor.com/iso.php
 [3]: http://packages.ubuntu.com/dapper/misc/casper
 [4]: http://www.realmtech.net/documents/DynamicDNS.pdf
 [5]: http://www.mattfoster.clara.co.uk/ddns.htm
