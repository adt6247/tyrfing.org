Tyrfing's Android Goodness
==========================
I'm a really big fan of android. I decided to compile here all of my 
findings with android.

I have a G1. I've since rooted it and am now running the cyanogen froyo 
rom CM6. It is great. I reccomend it. I partially created this list to 
keep track of what I installed when I blow out configurations and 
reinstall. It should be kept resonably upto date , and is a live 
document. I have divided the list of apps into catgories suchas 
requires root. I hope this is helpful.

I'm also have a debian chroot running on my phone for improved shell 
access. I'm a firm believe in compcache and will probably add a swap 
partition back to my phone soon.

Top Apps
------------------
- Google Maps by Google Inc.
- Android-vnc-viewer
- Scientific Calculator by kreactive
- FB Reader this plus all the EPUBs from project gutenburg let's me read the classics
- Meebo is my favorite IM utility
- Mapdroyd lets you use pre downloaded Open Street Maps data. For when your 
  lost in the woods. It still can use GPS info as long as it can get satelite. 
- Wikidroyd lets you use pre downloaded wikipedia databases. For when you need 
  to research somthing away from data service.
- Countdown timer from everything android. It is simple, but does its job 
- Connectbot is the best ssh and terminal client for android I use it 
  constantly. It operates best if you put 
  [xterm-color]({base-url}comp/xterm-color) in the /etc/terminfo/x/ directory 
  and then export TERM=xterm-color (Note: I pulled this terminfo file from 
  Debian's Arm build)
- Flashlight by Devesh useful when you need light, but otherwise tiny and 
  simple.
- Textedit by Paul Mach is the best texteditor for android unfortunately that 
  isn't saying much, but it is good for taking quick notes to your sdcard. 
- Diceroller this is my app at somepoint this will be a link to the apk.
- Antennas this is a very interesting app for learning about the layout of the 
  network near you.
- The Weather Channel App is great, but I'm not a big widget guy. And I prefer 
  [weather.gov](http://www.weather.gov/)

Top Apps requiring root
-----------------------
- android-wifi-tether (probably the best reason to root your phone, this may 
  all change as froyo comes into light)
- SetCPU for Root Users by MichaelHuang
- I think the G1 needs swap to handle real multitasking. I just dd'd 
  a swapfile onto my sdcard. Formatted it with mkswap. And then added a 
  script that swapon the file at boot.
- Swapper2 has also gotten good reviews
- firerat over at the android forums deserves a shoutout. I don't 
  agree with some of his advice and the patch script seems a little heavy 
  handed, but I have used it. the sdext command is the best.

Top games
---------
- Jewels the bejewelled clone you know you love it
- Robodefense this kept me up a couple nights
- Frozen Bubble is a favortie of mine, but the conrolls are clunky on android
- Scummvm unfortunately I lost this when I went to froyo

Hopefully there will be more info here soon.
