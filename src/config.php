<?php 
require 'environment.php';

$site_config = array();

$site_config['@default'] = array(
							'base-url'		=> BASEURL,
							'site-title'	=> 'Tyrfing.org',
							'page-title'	=> 'Home',
							'template'		=> 'templates/template.html',
							'js-includes'	=> array(),
							'css-includes'	=> array( 'tyrfing-style.css' => 'all' ),
							'includes'		=> array(),
						);

$site_config['bookmark'] = array(
							'template'		=> 'templates/template_empty.html'
						);
?>
