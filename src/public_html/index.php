<?php
/************************************************************************
 *                     DO NOT MODIFY THIS FILE! 
 ************************************************************************/
include '../config.php';
include '../lib/controller.php';

if (!defined('BASEURL')) exit('No BASEURL variable found! Edit config.php');

$control = new Controller( BASEURL, $site_config, '..', '.', '../pages', JS_DEBUG, CSS_DEBUG );
$control->execute();
?>