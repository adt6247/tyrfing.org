<?php 
/************************************************************************
 *                     DO NOT MODIFY THIS FILE!
 ************************************************************************/

/**
 * Parser Class
 *
 * Highly derivative of CodeIgniter parser library:
 * http://codeigniter.com/user_guide/libraries/parser.html
 * 
 * Removed CI specific stuff, functionality I don't need, and made 
 * the whole thing stateless. 
 */
class Parser {

	public static $l_delim = '{';
	public static $r_delim = '}';

	/**
	 * Parse a template
	 *
	 * Parses pseudo-variables contained in the specified template view,
	 * replacing them with the data in the second param
	 */
	public static function parse( $template, $data )
	{
		if ($template == '')
		{
			return FALSE;
		}
		
		foreach ($data as $key => $val)
		{
			if (is_array($val))
			{
				$template = self::_parse_pair($key, $val, $template);
			}
			else
			{
				$template = self::_parse_single($key, (string)$val, $template);
			}
		}
		
		return $template;
	}
	
	/**
	 * Parse a template
	 *
	 * Parses pseudo-variables contained in the specified template view,
	 * replacing them with the data in the second param
	 */
	public static function parse_file( $template_file_name, $data )
	{
		$template = file_get_contents( $template_file_name );

		return self::parse( $template, $data );
	}

	/**
	 *  Parse a single key/value
	 */
	private static function _parse_single($key, $val, $string)
	{
		return str_replace(self::$l_delim.$key.self::$r_delim, $val, $string);
	}

	/**
	 *  Parse a tag pair
	 *
	 * Parses tag pairs:  {some_tag} string... {/some_tag}
	 */
	private static function _parse_pair($variable, $data, $string)
	{
		if (FALSE === ($match = self::_match_pair($string, $variable)))
		{
			return $string;
		}

		$str = '';
		foreach ($data as $row)
		{
			$temp = $match['1'];
			foreach ($row as $key => $val)
			{
				if ( ! is_array($val))
				{
					$temp = self::_parse_single($key, $val, $temp);
				}
				else
				{
					$temp = self::_parse_pair($key, $val, $temp);
				}
			}

			$str .= $temp;
		}

		return str_replace($match['0'], $str, $string);
	}

	/**
	 *  Matches a variable pair
	 */
	private static function _match_pair($string, $variable)
	{
		if ( ! preg_match("|" . preg_quote(self::$l_delim) . $variable . preg_quote(self::$r_delim) . "(.+?)". preg_quote(self::$l_delim) . '/' . $variable . preg_quote(self::$r_delim) . "|s", $string, $match))
		{
			return FALSE;
		}

		return $match;
	}
}
?>