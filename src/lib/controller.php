<?php 
/************************************************************************
 *                     DO NOT MODIFY THIS FILE! 
 ************************************************************************/

require 'parser.php';
require 'JSMinPlus.php';
require 'markdown.php';

/**
 * Front-controller Class
 */
class Controller 
{
	protected $baseurl;
	protected $config;
	protected $page_dir;
	protected $src_dir;
	protected $content_dir;
	protected $js_debug;
	protected $css_debug;
	protected $seo_data;
	protected $body_classes;
	protected $copyright_year;

	/**
	 * Constructor. Sets default values of member variables.
	 * 
	 * @param $baseurl - base URL of the site, e.g., http://localhost/foo/bar 
	 *                   or http://foo.com/bar
	 * @param $site_config - configuration object. Associative array containing arrays 
	 *                       of variables used by the parser. Must have at least one
	 *                       element, '@default'.
	 * @param $src_dir - path to the source root, either relative or absolute. 
	 *                   E.g., '../src' or '/var/www/my_project/src'
	 * @param $content_dir - path to the static content, either relative or absolute.
	 *                   E.g., '../src/public_html' or '/var/www/my_project/src/public_html'
	 * @param $page_dir - path to the location of the page content HTML files, either 
	 *                    relative or absolute. E.g., '../src/pages' or '/var/www/my_project/src/pages'
	 * @param $js_debug - boolean value; determines whether we're minifying/combining 
	 *                    JavaScript files. 
	 * @param $css_debug  - boolean value; determines whether we're minifying/combining 
	 *                    CSS files. 
	 */
	function __construct( $baseurl, $site_config, $src_dir, $content_dir, $page_dir, $js_debug=TRUE, $css_debug=TRUE )
	{	
		$this->baseurl = $baseurl;
		$this->src_dir = $src_dir;
		$this->content_dir = $content_dir;
		$this->page_dir = $page_dir;
		$this->config = $site_config;
		$this->js_debug = $js_debug;
		$this->css_debug = $css_debug;

		$this->body_classes = array();

		$current_year = @date('Y'); 
		$this->copyright_year = ('2012' != $current_year) ? "2012-$current_year" : '2012';

		$this->seo_data = array();
		if( ($handle = fopen( "$src_dir/seo.csv", 'r' )) !== FALSE ) 
		{
			while( ($row = fgetcsv($handle, 1000, ",")) ) 
			{
				list( $page_name, $page_title, $meta_description, $meta_keywords ) = $row;
				$this->seo_data[$page_name] = array( 
												'page-title'       => $page_title,
												'meta-description' => $meta_description,
												'meta-keywords'    => $meta_keywords
											);
			}
			fclose($handle);
		}
	}
	
	/**
	 * Start the front controller
	 */
	public function execute()
	{
		$extension = '';
		if( array_key_exists('QUERY_STRING', $_SERVER ) )
		{
			$query_string = substr( $_SERVER['QUERY_STRING'], 1 );
		}
		else
		{
			$query_string = '';
		}

		$dir_index = false;
		if( empty($query_string) || ($query_string=='/') )
		{
			$page_name = 'index';
		}
		else
		{
			$filename_pieces = explode( '.', $query_string );
			if( count( $filename_pieces > 1) )
			{
				$extension = strtolower( $filename_pieces[count($filename_pieces) - 1] );
			}
			$page_name = trim($query_string, '/');
			if( is_dir("$this->page_dir/$page_name") ) {
				$dir_index = true;
				$dir_to_index = $page_name;
				$page_name .= '/index';
			}
		}

		switch( $extension )
		{
			case 'html':
				// trim '.html' from the pagename so we can optionally do with
				// or without it.
				$page_name = substr( $page_name, 0, strlen($page_name) - 5 );
				break;
			case 'php':
				// in case '.php' is the extension in the URL, redirect to the 
				// URL with '.php' removed. Old site used '.php' extension; 
				// redirect is for SEO purposes
				$page_name = substr( $page_name, 0, strlen($page_name) - 4 );
				header("HTTP/1.1 301 Moved Permanently"); 
				header( "Location: $this->baseurl$page_name" );
				die();
		}

		switch( $page_name )
		{
			case 'sitemap.xml':
				header( "Content-type: application/xml ");
				echo Parser::parse_file( "$this->src_dir/templates/sitemap.xml", 
										 array('pages'=>$this->get_sitemap_data() ) );
				break;
			default:
				$file_name_html = "$this->page_dir/$page_name.html";
				$file_name_md = "$this->page_dir/$page_name.md";
				$file_name_other = "$this->page_dir/$page_name";
				$use_markdown = false;
				$config = $this->get_page_config( $page_name );
				$this->add_body_class( str_replace('/', ' ', $page_name ) );
				
				if( file_exists( $file_name_html ) )
				{
					$file_name = $file_name_html;
				}
				else if( file_exists( $file_name_md ) )
				{
					$file_name = $file_name_md;
					$use_markdown = true;
				}
				else if( file_exists( $file_name_other ) )
				{
					switch( $extension )
					{
						case 'txt':
							header( 'Content-type: text/plain' );
							break;
						case 'pdf':
							header( 'Content-type: application/pdf' );
							break;
						case 'jpg':
						case 'jpeg':
							header( 'Content-type: image/jpeg' );
							break;
						case 'gif':
							header( 'Content-type: image/gif' );
							break;
						case 'png':
							header( 'Content-type: image/png' );
							break;
						default:
							header( 'Content-Disposition: attachment' );

					}
					readfile( $file_name_other );
					die();
				}
				else if ( $dir_index )
				{
					$file_name = "$this->page_dir/dir_index.md";
					$use_markdown = true;
				}
				else
				{
					$file_name = "$this->page_dir/error_404.md";
					$use_markdown = true;
					header('HTTP/1.0 404 Not Found');
				}

				$config['query-string'] = $query_string;

				if ( $dir_index )
				{
					$config['dir-index'] = $this->get_dir_index( $dir_to_index );
				}

				$segments = explode( '/', $page_name );

				if( $page_name == 'sitemap' )
				{
					$config['pages'] = $this->get_sitemap_data();
				}
				$template = $this->src_dir . '/' . $config['template'];
				
				// serve as XML if user agent accepts it.
				if( (ENVIRONMENT == 'PRODUCTION') && isset($_SERVER["HTTP_ACCEPT"]) && 
					stristr( $_SERVER["HTTP_ACCEPT"], "application/xhtml+xml") ) 
				{
					header ("Content-type: application/xhtml+xml");
				} 
				else 
				{
					header ("Content-type: text/html");
				}
				
				echo Parser::parse_file( $template, $this->get_parser_data( $file_name, $config, $use_markdown ) );
		}		
	}

	/**
	 * Adds a class to the body of the rendered page.
	 * @param $class - name of the class we're adding to the body.
	 */
	protected final function add_body_class( $class )
	{
		array_push( $this->body_classes, $class );
	}

	/**
	 * Creates the data object to be used by the parser to construct the page. 
	 * Includes rendered CSS and JavaScript files, page content, and any variables 
	 * defined in the $config object.
	 *
	 * @param $file_name - path to the file used to fill the page content.
	 * @param $config - object containing configuration data for the page.
	 */
	private function get_parser_data( $file_name, $config, $use_markdown=false )
	{
		$data = $config;

		// grab all includes
		if( array_key_exists('includes', $config) )
		{
			foreach( $config['includes'] as $var_name => $include_file_name )
			{
				$data[$var_name] = Parser::parse_file( "$this->src_dir/$include_file_name", $data );
			}
		}
		
		// if body classes are defined, make them into a list of classes.
		if( count($this->body_classes) > 0 )
		{
			$data['body-classes'] = 'class="' . implode( ' ', $this->body_classes ) . '"';
		}
		else
		{
			$data['body-classes'] = '';
		}

		// grab page content
		$page_content = file_get_contents( $file_name );
		if( $use_markdown )
		{
			$page_content = Markdown( $page_content );
		}		
		if( preg_match( '#<h1[^>]*>(.*?)</h1>#i', $page_content, $h1_tags ) )
		{
			$data['page-title'] = $h1_tags[1];
		}
		$data['page-content'] = Parser::parse( $page_content, $data );

		$data['css-includes'] = $this->render_css_includes( $config['css-includes'] );
		
		$data['js-includes'] = $this->render_js_includes( $config['js-includes'] );
		
		$data['copyright-year'] = $this->copyright_year;
		
		return $data;
	}

	/**
	 * Flattens the page config, merging the default page configuration with the 
	 * page-specific configuration, if extant.
	 *
	 * For instance, default exists at $this->config['@default']. If the page name 
	 * is 'foo', There may be a $this->config['foo']. If 'foo' has properties, they
	 * will be added to the default. If both have the same property, 'foo' will
	 * over-write the default in the resulting object.
	 * 
	 * @param $page_name - name of the page entry we're looking for in $this->config
	 *                     array.
	 * 
	 * @return an associative array that has the intersection between 
	 *         $this->config['@default'] and $this->config[$page_name]
	 */
	private final function get_page_config( $page_name )
	{
		$config = $this->config['@default'];
		
		$this->config['index']['nav'] = '';

		if( array_key_exists($page_name, $this->config) )
		{
			$page_config = $this->config[$page_name];
			
			foreach( $page_config as $key => $value )
			{
				$config[$key] = $value;
			}
		}

		$seo = $this->seo_data['@default'];
		if( array_key_exists($page_name, $this->seo_data) )
		{
			$page_seo = $this->seo_data[$page_name];
			
			foreach( $page_seo as $key => $value )
			{
				if( !empty($value) )
				{
					$seo[$key] = $value;
				}
			}
		}
		
		$meta_tags = "\n";
		if( isset($seo['meta-description']) )
		{
			$desc = $seo['meta-description'];
			$meta_tags .= "\t<meta name=\"description\" content=\"$desc\" />\n";
		}
		if( isset($seo['meta-keywords']) )
		{
			$key = $seo['meta-keywords'];
			$meta_tags .= "\t<meta name=\"keywords\" content=\"$key\" />\n";
		}
		$config['meta-tags'] = $meta_tags;

		$config['page-name'] = $page_name;

		////////////////////////////////////////////////////////////////
		// Rudimentary tracking
		$converted_page_name = str_replace('/', '_', $page_name );
		$tracking_filename = "$this->src_dir/tracking/$converted_page_name.cnt";
		if( file_exists( $tracking_filename ) )
		{
			$tr_file = fopen( $tracking_filename, 'r');
			$tr_date = fgets( $tr_file, 11 );
			$tr_count = fgets( $tr_file );
			fclose($tr_file);
		}
		else
		{
			$tr_date = date( 'Y-m-d' );
			$tr_count = 0;
		}
		$tr_count = $tr_count + 1;
		$tr_file = fopen( $tracking_filename, 'w');
		if ( $tr_file )
		{
			fwrite( $tr_file, $tr_date );
			fwrite( $tr_file, $tr_count );
			fclose( $tr_file );
		}

		$config['tracking-count'] = $tr_count;
		$config['tracking-date'] = $tr_date;
		// End Rudimentary tracking
		////////////////////////////////////////////////////////////////

		return $config;
	}
		
	/**
	 * Return true if any file in $file_list is younger than the $test_file.
	 * Returns true is $test_file does not exist
	 * 
	 * @param $test_file - The name of the putative oldest file.
	 *                     Its name is relative to the directory, so watch out for that
	 * @param $file_list - List of file names to test if younger than $test_file. 
	 *                     Names added to directory
	 *
	 * @param $directory - directory all of these files are in
	 * @return true if any file named in $file_list younger than $test file or if $test_file does not exist.
	 */
	private final function anything_younger( $test_file, $file_list, $directory = '' )
	{
		$directory = rtrim($directory,'/');
		if(empty($directory))
		{
			$directory = '.';
		}
		$directory .= '/';
		if(!file_exists($directory.$test_file))
		{
			return true;
		}
		$test_file_mtime = @filemtime( $directory.$test_file );
		foreach( $file_list as $file )
		{
			if( $test_file_mtime < filemtime($directory.$file) )
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Minify the contents of a CSS file
	 * @param - $css_text the text of a CSS file that has not been minified
	 * @return that CSS string minified.
	 */
	private final function css_minify($css_text)
	{
		$replacements = array(
			'|[\n\r]|'                => '',   //newlines
			'|[; ]*([;:,>{}])[ \t]*|' => '\1', //whitespace/semicolons around brackets
			'|[ \t]+|'                => ' ',  //superfluous whitespace
			'|/\*.+?\*/|'             => ''   //comments
		);
		return preg_replace(array_keys($replacements), array_values($replacements), $css_text);
	}

	/**
	 * Process/minify all CSS files for this page, and return a list of
	 * <code>&lt;link&gt;</code> elements to inject into the HTML document
	 * which include the rendered CSS files.
	 *
	 * Uses the CSS Crush preprocessor to perform the minifying and processing.
	 *
	 * @param $css_file_names - list of CSS file names to process and include
	 *                          in the HTML document being rendered.
	 * @return a list of <code>&lt;link&gt;</code> elements to inject into 
	 *         the HTML document which include the rendered CSS files.
	 */
	private final function render_css_includes( $css_file_names )
	{
		if( count( $css_file_names ) == 0 ){
			return '';
		}

		$options = array();
		$css_includes = '';
		
		if( $this->css_debug === true )
		{
			$css_dir = $this->baseurl . 'css';
			foreach( $css_file_names as $file => $media )
			{
				$media = (empty($media)) ? 'all' : $media;
				$css_includes .= "	<link rel=\"stylesheet\" media=\"$media\" href=\"$css_dir/$file\" />\n";
			}
		}
		else
		{
			$css_dir = "$this->content_dir/css";

			$output_dir = "$this->content_dir/css";
			$output_filename = md5(implode($css_file_names)).'.cssmin.css';
			if( $this->anything_younger( $output_filename, array_flip( $css_file_names ), $css_dir ) )
			{
				$css_code = '';
				foreach( $css_file_names as $file => $media )
				{
					$media = (empty($media)) ? 'all' : $media;
					$temp = file_get_contents( "$css_dir/$file" );
					if( !strpos( $file, '.min.' ) && !strpos( $file, '.nomin.' ) ) // if not already minified
					{
						$temp = $this->css_minify( $temp );
	  				}
	  				if( $media != 'all')
	  				{
	  					$css_code .= "/* File: $css_dir/$file */\n@media ($media) {\n\t$temp\n}\n";
	  				}
	  				else
	  				{
						$css_code .= "/* File: $css_dir/$file */\n$temp\n";
					}
					
				}
				$success = file_put_contents( "$output_dir/$output_filename", $css_code );
			}
			$css_include_filename = $this->baseurl . 'css/' . $output_filename;
			$css_includes = "	<link rel=\"stylesheet\" href=\"$css_include_filename\" />\n";
		}		
		
		return $css_includes;
	}
	
	/**
	 * Minify all JavaScript files for this page, and return a list of
	 * <code>&lt;script&gt;</code> elements to inject into the HTML document
	 * which include the rendered JavaScript files.
	 *
	 * Uses the JSMin preprocessor to perform the minifying.
	 *
	 * @param $js_file_names - list of JavaScript file names to process and 
	 *                         include in the HTML document being rendered.
	 * @return a list of <code>&lt;script&gt;</code> elements to inject into 
	 *         the HTML document which include the rendered JavaScript files.
	 */
	private final function render_js_includes( $js_file_names )
	{
		if( count( $js_file_names ) == 0 ){
			return '';
		}

		$js_includes = '';
		
		if( $this->js_debug === true )
		{
			foreach( $js_file_names as $file )
			{
				$js_includes .= '<script src="' . $this->baseurl . 'js/' . $file . '"></script>' . "\n\t";
			}
			$js_includes .= "<!-- JS_DEBUG MODE -->\n";
		}
		else 
		{
		  $output_filename = md5(implode($js_file_names)).'.jsmin.js';
		  if($this->anything_younger($output_filename,$js_file_names,"$this->content_dir/js/"))
		  {
  			$js_code = '';
  			foreach( $js_file_names as $file )
  			{
  				$temp = file_get_contents( "$this->content_dir/js/$file" );
  				if( !strpos( $file, '.min.' ) && !strpos( $file, '.nomin.' ) ) // if not already minified
  				{
  					$temp = JSMinPlus::minify( $temp );
  				}
  				$js_code .= "/* File: $file */\n$temp\n";
  				//$js_includes .= "<!-- JS FILE INCLUDED = $file -->\n";
  				
  			}
  			$success = file_put_contents( "$this->content_dir/js/$output_filename", $js_code );
  			//$js_includes .= "<!-- JSMin bytes written = $success -->\n";
  		}
  		$js_includes .= '<script src="' . $this->baseurl . "js/$output_filename\"></script>";
    }
		return $js_includes;
	}

	/**
	 * Get listing of files that we can serve up in a directory.
	 * 
	 * @param $path - path of the directory we're trying to get an index of
	 * @return data object used to parse a directory index template
	 */
	protected final function get_dir_index( $path )
	{
		$index_data = array();
		$dir_to_open = $this->page_dir . '/' . $path;
		if( $handle = opendir( $dir_to_open ) )
		{
			while( false !== ($entry = readdir($handle)) )
			{
				if( substr( $entry, 0, 1) != '.' ) 
				{
					$name = $entry;
					if( is_dir($this->page_dir . $path . '/' . $entry) ) {
						$name = $name . '/';
					}
					$index_data["$path$entry"] = array(
									'url' => $this->baseurl . $path . '/' . $entry,
									'name' => $name
								);
				}
			}
		}

		ksort( $index_data );
		return $index_data;
	}

	/**
	 * Constructs a data object for use with parsing a sitemap.
	 * 
	 * @param $path - path of the pages that we're looking at
	 * @param $file_extension - the file extension that the rendered pages will 
	 *                          use. E.g.: '.html', '.php', ''.
	 * @return data object used to parse a sitemap template
	 */
	protected final function get_sitemap_data( $path='', $file_extension = '', $css_class='indent' )
	{
		$sitemap_data = array();
		$dir_to_open = $this->page_dir . '/' . $path;
		if( $handle = opendir( $dir_to_open ) )
		{
			$pages = array();
			if( $path == '' )
			{
				$pages[''] = array(
								'url' => $this->baseurl,
								'name' => "Home",
								'timestamp' => filemtime("$this->page_dir/index.html"),
								'css-class' => 'home'
							);
			}
			while( false !== ($entry = readdir($handle)) )
			{
				if( ($entry != 'index.html' ) && (substr($entry, -5) == '.html') )
				{
					$file_name = str_replace( '.html', $file_extension, $entry );
					if( substr($file_name, 0, 5) != 'error' )
					{
						$pages["$path$file_name"] = array(
										'url' => $this->baseurl . $path . $file_name,
										'name' => $path . $file_name,
										'timestamp' => filemtime("$this->page_dir/$path$entry"),
										'css-class' => "$css_class $file_name"
									);
					}
				} 
				else if( ($entry != '.') && ($entry != '..') && (is_dir($this->page_dir . $path . '/' . $entry)) ) 
				{
					$pages["$path$entry"] = array(
									'url' => $this->baseurl . $path . $entry,
									'name' => $path . $entry,
									'timestamp' => filemtime("$this->page_dir/$path$entry"),
									'css-class' => "$css_class $entry"
								);
					$dir_merge = $this->get_sitemap_data( "$path$entry/", $file_extension, 'indent2' );
					$pages = array_merge( $pages, $dir_merge );
				}
			}
			$sitemap_data = $pages;
		}

		ksort( $sitemap_data );
		return $sitemap_data;
	}
}
?>